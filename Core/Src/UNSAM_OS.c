/*
 * UNSAM_OS.c
 *
 *  Created on: May 12, 2021
 *      Author: UNSAM EDIII
 *      Source destinado al Kernel de nuestro sistema Operativo
 */

#include <UNSAM_OS.h>


void (*Task_Pointer [TASKS_MAXIMAS]) (void);

void Scheduling (void)
{
	static unsigned int Task_Actual = 0;

	if (Task_Actual < TASKS_MAXIMAS  && Task_Pointer[Task_Actual] != 0)
		Task_Pointer [Task_Actual]();

	//else
	//{
	//	while (Task_Pointer[Task_Actual] == 0  && Task_Actual++ < ( TASKS_MAXIMAS - 1) );
	//}


	if (Task_Actual++ >= ( TASKS_MAXIMAS - 1))
		Task_Actual = 0;
}


unsigned int Task_Add (void (*Task_To_Add) (void))
{
	unsigned int i = 0;

	while (Task_Pointer[i] != 0  &&  i < TASKS_MAXIMAS)
		i++;

	if (i < TASKS_MAXIMAS)
	{
		Task_Pointer[i] = Task_To_Add;
		return i;
	}

	return 0xFF;
}
