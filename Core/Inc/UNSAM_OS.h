/*
 * UNSAM_OS.h
 *
 *  Created on: May 12, 2021
 *      Author: UNSAM EDIII
 *      Header destinado al Kernel de nuestro sistema Operativo
 */

#ifndef INC_UNSAM_OS_H_
#define INC_UNSAM_OS_H_

#endif /* INC_UNSAM_OS_H_ */

#define	TASKS_MAXIMAS	5

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Exported functions prototypes ---------------------------------------------*/
void Scheduling(void);
unsigned int Task_Add (void (*) (void));
