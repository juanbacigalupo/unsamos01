/*
 * Tasks.h
 *
 *  Created on: May 12, 2021
 *      Author: Juan
 *      Header destinado al trabajo con tareas de nuestro Sistema Operativo (RTOS).
 */

#ifndef INC_TASKS_H_
#define INC_TASKS_H_

#endif /* INC_TASKS_H_ */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
/* USER CODE END Includes */


/* Exported constants --------------------------------------------------------*/
#define CANTIDAD_ENTRADAS	1
#define CANTIDAD_SALIDAS	1
#define THRESHOLD			10

/* Exported functions prototypes ---------------------------------------------*/
void Lectura_Entradas (void);
void Escritura_Salidas (void);
void Error (void);
